![image.png](./preview.jpg)

## 关于我们
该模板有潍坊雷鸣云网络科技有限公司 www.leimingyun.com 开发共享，我公司主要从事xxxx方面业务，欢迎大家合作

## 模板简介
预览网址: [http://www.zhongbing.zvo.cn](http://www.zhongbing.zvo.cn/)  
支持终端: 手机端、电脑端

## 使用步骤
1. 登录您 [网市场云建站系统](https://gitee.com/mail_osc/wangmarket)  的总管理后台（使用admin账号登录）
2. 找到[功能插件-模板](http://www.wang.market/10121.html)，进入其中的模板管理
3. 将该模板文件 (template.zip) 导入
4. 将导入的这一条模板，设置其 模版性质 这个属性为公开
5. 然后再开通一个新网站，选择模板时，即可看到这个了

## 分享模板注意事项
#### 仓库命名方面
命名方面要以 wangmarket_template_ 为前缀，如我们这个模板，模板名为 lmyglm1 ，那么创建的开源仓库的命名便是 wangmarket_template_lmyglm1
#### 仓库文件方面
* template.zip  使用模板制作插件 http://tag.wscso.com/4192.html 做好后导出的模板文件
* preview.jpg    网站预览图，也就是template.zip中的那个 preview.jpg 预览图
* README.md    说明文件，直接复制这个文件格式即可
#### README.md 说明
直接复制本 README.md 的格式即可。当然，这个 分享模板注意事项 最后这条就不用加上了。
* **关于我们**，可以写一下您这边的一些介绍、或者擅长的点了，也便于让人看了给您这边带来业务等。
* **模板简介**，主要就是写一下当前模板预览的地址，方便访客能直接点开看这个模板；支持终端则是当前模板是单纯PC端呢，手机电脑通用的响应式呢，支持哪些终端
* **使用说明**，无需改动，保持原样即可。